package GameTest
{
	import flash.display.Sprite;
    import starling.core.Starling;
	
	/**
	 * ...
	 * @author Arturo
	 */
		
 
    [SWF(width = "800", height = "600", frameRate = "60", backgroundColor = "#FFFFFF",wmode="direct")]
	
    public class Main extends Sprite
    {
        private var starling:Starling;
 
        public function Main()
        {
            // Create a Starling instance that will run the "Game" class
            starling = new Starling(Game, stage);
            starling.start();
        }
		
    }
	
}