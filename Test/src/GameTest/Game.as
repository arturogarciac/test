package GameTest 
{
	import flash.events.TimerEvent;
	import flash.geom.Point;
	import flash.geom.Rectangle;
	import flash.utils.Timer;
	import starling.display.Button;
	import starling.events.Event;
	import starling.events.EnterFrameEvent;
	import starling.display.Sprite;
	import starling.text.TextField;
	import starling.textures.Texture;
	import starling.utils.Color;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.utils.HAlign;
	

	/**
	 * ...
	 * @author Arturo
	 */
	public class Game extends Sprite
	{
		
		[Embed(source="/../Assets/Textures/Play-button.png")]
		public static const playButtonImage:Class;
		
		private var timeText:TextField;
		private var scoreText:TextField;
		private var score:int;
		
		private var cubeSpeed:Number;
		
		private var myTimer:Timer;
		private var totalSeconds:int;
		private var seconds:int;
		private var minutes:int;
		
		private var cube:Cube;
		private var box1:Cube;
		private var box2:Cube;
		
		private var initCubePositionX:Number;
		private var initCubePositionY:Number;
		private var minPositionX:Number;
		private var maxPositionX:Number;
		private var initBoxPositionY:Number;
	
		private var touch:Touch;
		
		private var playButton:Button;
		
		private var firstPlay:Boolean = false;
		
		public function Game() 
		{
			this.addEventListener(Event.ADDED_TO_STAGE, OnAddedToStage);

		}
		
		private function OnAddedToStage(e:Event):void 
		{
			this.removeEventListener(Event.ADDED_TO_STAGE, OnAddedToStage);
			
			cubeSpeed = 100;
			initCubePositionX = stage.stageWidth / 2;
			initCubePositionY = -100;
			minPositionX = 200;
			maxPositionX = 600;
			initBoxPositionY = stage.stageHeight - 100;
			
			CreatePlayButton();
			
		}
		
		private function OnPlayButtonClick(e:Event):void 
		{
			Initialize();
			
			playButton.visible = false;
			ShowGameElements(true);

			myTimer.start();
			
		}
		
		private function Initialize():void
		{
			score = 0;
			totalSeconds = 90;
			
			this.addEventListener(TouchEvent.TOUCH, OnTouch);
			this.addEventListener(EnterFrameEvent.ENTER_FRAME, OnEnterFrame);
			
			//Solo creamos los elementos en la primera partida
			if (!firstPlay)
			{
				CreateCubes();
				CreateScore();
				CreateTimer();
			}
			
			InitCubes();
			UpdateScore();
			timeText.text = FormatTime(totalSeconds);
		}
		
		private function CreatePlayButton():void
		{
			var buttonTexture:Texture = Texture.fromEmbeddedAsset(playButtonImage);
			playButton = new Button(buttonTexture);
			playButton.alignPivot();
			playButton.x = stage.stageWidth / 2;
			playButton.y = stage.stageHeight / 2;
			this.addChild(playButton);
			
			this.addEventListener(Event.TRIGGERED, OnPlayButtonClick);
			
		}
		
		private function GameOver():void 
		{
			myTimer.stop();
			playButton.visible = true;
			ShowGameElements(false);
			
			//No se si es necesario estar quitando estos listeners
			//this.removeEventListener(TouchEvent.TOUCH, OnTouch);
			//this.removeEventListener(EnterFrameEvent.ENTER_FRAME, OnEnterFrame);
			
			firstPlay = true;
			
			trace("GAME OVER");
			
		}
		
		private function OnEnterFrame(e:EnterFrameEvent):void 
		{
			//variamos ligeramente la velocidad en función del score
			cube.y += (cubeSpeed + score/2) * e.passedTime;
			
			if (cube.y > stage.stageWidth){
				
				RemovePoints(5);
				ResetCubes();
			}
			
			CheckCollisions();
			
		}
		
		private function ShowGameElements(value:Boolean):void
		{
			cube.visible = value;
			box1.visible = value;
			box2.visible = value;
			scoreText.visible = value;
			timeText.visible = value;
		}
		
		//Si colisionan dos cubos
		private function CheckCollisions():void
		{
			
			if (cube.bounds.intersects(box1.bounds))
			{
				
				//Verificamos si el color coincide para sumar o restar puntos
				if (cube.GetColor() == box1.GetColor())
				{
					 AddPoints(10);
					
				} else RemovePoints(5);
				
				ResetCubes();
				
			}
			
			if (cube.bounds.intersects(box2.bounds))
			{
				
				//Verificamos si el color coincide para sumar o restar puntos
				if (cube.GetColor() == box2.GetColor())
				{
					 AddPoints(10);
					
				} else RemovePoints(5);
				
				ResetCubes();
				
			}
			
		}
		
		private function CreateCubes():void
		{
			cube = new Cube();
			addChild(cube);
			
			box1 = new Cube();
			addChild(box1);
			
			box2 = new Cube();
			addChild(box2);
	
		}
		
		private function InitCubes():void
		{
			cube.x = initCubePositionX;
			cube.y = initCubePositionY;
			
			box1.x = maxPositionX;
			box1.y = initBoxPositionY;
			
			box2.x = minPositionX;
			box2.y = initBoxPositionY;
			
			ResetCubes();
		}
		
		private function ResetCubes():void
		{
			//Reseteamos la posición del cubo
			cube.x = stage.stageWidth / 2;
			cube.y = -100;
			
			//Cambiamos el color de las cajas de tal manera que no coincidan
			box1.ChangeColor();
			if (box1.GetColor() < 5) box2.SetColor(box1.GetColor() + 1);
			else box2.SetColor(box1.GetColor() - 1);
			
			//Cambiamos el color del cubo para que coincida con una de las cajas
			var boxNumber:Number = Math.random();
			
			if (boxNumber < 0.5) cube.SetColor(box1.GetColor());
			else cube.SetColor(box2.GetColor());
	
		}
		
		private function AddPoints(points:int):void
		{
			score += points;
			UpdateScore();
			
			trace("Suma puntos");
		}
		
		private function RemovePoints(points:int):void
		{
			score -= points;
			
			if (score < 0) score = 0;
			
			UpdateScore();
			
			trace("Resta puntos");
			
		}
		
		private function UpdateScore():void 
		{
			scoreText.text = score.toString();
		}
		
		
		private function CreateScore():void 
		{
			scoreText = new TextField(150, 40, score.toString(), "Verdana", 32);
			scoreText.x = 0;
			scoreText.y = 0;
			scoreText.border = true;
			scoreText.hAlign= HAlign.CENTER
			addChild(scoreText);
			
		}
		
		private function CreateTimer():void 
		{
			timeText = new TextField(150, 40, "" , "Verdana", 32);
			timeText.x = stage.stageWidth - 150;
			timeText.y = 0;
			timeText.border = true;
			timeText.hAlign= HAlign.CENTER
			addChild(timeText);
			
			myTimer=new Timer(1000)  
			myTimer.addEventListener(TimerEvent.TIMER,UpdateTimer)
			
			
		}

		private function UpdateTimer(e:TimerEvent):void 
		{
			totalSeconds--;
			
			if (totalSeconds <= 0) GameOver();
			
			timeText.text = FormatTime(totalSeconds);
			
		}
		
		//Devuelve el tiempo con una estructura minutos:segundos -> 00:00 
		private function FormatTime(i:int):String
		{
			
			var text:String = "";
			
			if (i > 59) {
				
				minutes = Math.floor(i / 60);
				seconds = totalSeconds % 60;
				
				if (seconds < 10) text = String("0" + minutes + ":0" + seconds);
				else text = String("0" + minutes + ":" + seconds);

			} 
			else
			{
				seconds = i;
				
				if (seconds < 10) text = "00:0" + seconds.toString();
				else text = "00:" + seconds.toString();				
			}
			
			return text;
			
		}
		
		private function OnTouch(e:TouchEvent):void 
		{
			touch = e.getTouch(cube, TouchPhase.MOVED);
			
			if (touch)
			{
				
				var cubePoint:Point = new Point(cube.x, cube.y);
				var touchPoint:Point = new Point(touch.globalX, touch.globalY);
				
				//si la distancia entre el toque y el cubo es pequeña (el dedo/raton esta encima), podemos mover el cubo dentro de los limites
				if (Point.distance(touchPoint,cubePoint) < 100)
				{

					if (touch.globalX < 200)
					{
						cube.x = 200;
						
					}
					else if (touch.globalX > 600)
					{
						cube.x = 600
					}
					else
					{	
						cube.x = touch.globalX;
					}
				}
				
			} else {
				
				FixCube();
							
			}
					
		}
		
		private function FixCube():void
		{
			
			if (cube.x > initCubePositionX && cube.x < maxPositionX)
			{
				cube.x = maxPositionX;
				
			}
			else if  (cube.x > minPositionX && cube.x < initCubePositionX)
			{
				
				cube.x = minPositionX;
			}
			
		}
		
		
		
	}

}