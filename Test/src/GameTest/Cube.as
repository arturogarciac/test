package GameTest 
{
	import starling.events.Event;
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.utils.Color;
	
	/**
	 * ...
	 * @author Arturo
	 */
	public class Cube extends Sprite 
	{
		private var image:Quad;
		private var color:int;
		
		public function Cube() 
		{
			super();
			
			image = new Quad(100, 100, Color.WHITE);
			this.ChangeColor();
			image.alignPivot();
			addChild(image);
		}
		
		public function ChangeColor():void
		{
			//Valor minimo y máximo
			var min:int = 1;
			var max:int = 5;
			
			//Obtenemos un numero entre 1 y 5
			var colorNumber:int = Math.floor( Math.random() * (max - min + 1) + min);
			
			SetColor(colorNumber);
		}
		
		public function SetColor (colorNumber:int):void
		{
			var newColor:uint = Color.WHITE;
			
			//En funcion del numero, cambiamos a un color predefinido
			switch (colorNumber)
			{
				case 1:
					newColor = Color.BLUE;
					break;
				case 2:
					newColor = Color.FUCHSIA;
					break;
				case 3:
					newColor = Color.RED;
					break;
				case 4:
					newColor = Color.GREEN;
					break;
				case 5:
					newColor = Color.GRAY;
					break;
					
			}
			
			color = colorNumber;
			image.color = newColor;
			
			
		}
		
		public function GetColor():int 
		{
			return color;
		}
		
	
		
		
	}

}